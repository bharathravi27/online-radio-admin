import Vue from 'vue'
import App from './App.vue'
import Main from './components/main.vue'
import VueResource from 'vue-resource'
import vueRouter from 'vue-router'
import Routes from './router'

Vue.use(VueResource);
Vue.use(vueRouter);
const router = new vueRouter({
  routes: Routes
});
new Vue({
  el: '#app',
  render: h => h(App),
  router: router
})

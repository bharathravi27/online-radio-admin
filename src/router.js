import Add from './components/add.vue'
import Main from './components/main.vue';
import Edit from './components/edit.vue'

export default [
  {path:'/',
  component: Main},
  {path: '/add',
  component: Add},
  {path: '/edit',
  component: Edit}
]
